TENNIS_POINTS = {0: "Love", 1: "15", 2: "30", 3: "40", 4: "adv"}

DEUCE = "Deuce"

MIN_GAP = 2
MIN_POINTS_FOR_GAME = 4
MIN_POINTS_FOR_TIEBREAK_GAME = 7
DEUCE_POINTS = 3

MIN_GAMES_FOR_SET = 6
TIEBREAK_SET_LEVEL = 6

MIN_SETS_FOR_MATCH = 3

PLAYER = "Player"
POINTS = "Points"
GAMES = "Games"
SETS = "Sets"
PAST_SETS = "Past Sets"

class Player:
    def __init__(self, name):
        self.name = name
        self.points = 0
        self.games = 0
        self.sets = 0
        self.past_sets_games = []

    def __eq__(self, other):
        return self.name == other.name
    
    def __str__(self):
        return f'{self.name}'

    def archive_past_set_games(self):
        self.past_sets_games.append(self.games)

class Match:
    def __init__(self, a_player: Player, b_player: Player):
        self.a_player = a_player
        self.b_player = b_player
        self.game_won = False
        self.deuce = False
        self.tiebreak_round = False


    def __str__(self):

        return f'''
{PLAYER:30} - {POINTS:7} - {GAMES:7} - {SETS:7} - {PAST_SETS:15}
{self.a_player.name:30} - {self._rep_points(self.a_player.points):7} - {self.a_player.games:7} - {self.a_player.sets:7} - {self.a_player.past_sets_games}
{self.b_player.name:30} - {self._rep_points(self.b_player.points):7} - {self.b_player.games:7} - {self.b_player.sets:7} - {self.b_player.past_sets_games}'''
    
    def _rep_points(self, points):
        if not self.tiebreak_round:
            return DEUCE if self.deuce else TENNIS_POINTS[points]
        else:
            return str(points)    
    
    def award_point(self, winner: Player):
        if self.game_won:
            raise ValueError(f"Game, set, and match - already {winner}")        
        elif self.tiebreak_round:
            self.award_tiebreak_point(winner)
            return
        
        winner.points += 1
        pointses = self.a_player.points, self.b_player.points
        trail, lead = min(pointses), max(pointses)
        
        if lead - trail >= MIN_GAP and lead >= MIN_POINTS_FOR_GAME:
            self.deuce = False 
            self.award_game(winner)
        
        elif lead == trail and lead >= DEUCE_POINTS:
            self.deuce = True
            self.a_player.points, self.b_player.points = DEUCE_POINTS, DEUCE_POINTS #Tennis "resets" back to deuce

    def award_tiebreak_point(self, winner: Player):
        winner.points += 1
        pointses = self.a_player.points, self.b_player.points
        trail, lead = min(pointses), max(pointses)

        if lead - trail > MIN_GAP and lead >= MIN_POINTS_FOR_TIEBREAK_GAME:
            winner.games += 1
            self.tiebreak_round = False
            self.a_player.points, self.b_player.points = 0, 0
            self.award_set(winner)
    
    def award_game(self, winner: Player):
        self.a_player.points = 0
        self.b_player.points = 0    
        winner.games += 1
        gameses = self.a_player.games, self.b_player.games
        trail, lead = min(gameses), max(gameses)

        if lead - trail >= MIN_GAP and lead >= MIN_GAMES_FOR_SET:
            self.award_set(winner)

        elif lead == trail == TIEBREAK_SET_LEVEL:
            self.tiebreak_round = True
    

    def award_set(self, winner: Player):
        self.a_player.archive_past_set_games()
        self.b_player.archive_past_set_games()
        self.a_player.games, self.b_player.games = 0, 0    
        winner.sets += 1
        setses = self.a_player.sets, self.b_player.sets
        trail, lead = min(setses), max(setses)
        if lead == MIN_SETS_FOR_MATCH:
            self.game_won = True

hingis = Player('Hingis')
navratilova = Player('Navratilova')

semifinal = Match(hingis, navratilova)

hingis.points, hingis.games, hingis.sets = 3, 6, 0
navratilova.points, navratilova.games, navratilova.sets = 2, 4, 0

semifinal.award_point(hingis)
semifinal.award_point(navratilova)

print(semifinal)
